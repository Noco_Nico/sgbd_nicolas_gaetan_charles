package fr.afpa.controle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ControleAuthentification {
	
	private BufferedReader chercherFichier(String file) {
		FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			return br;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean controleLogin(JTextField login1) {
		
			BufferedReader br = chercherFichier("C:\\ENV\\SBDD\\LoginMdp.txt");
			
			String ligne ;
			String loginAdminEntre = login1.getText();
			
			try {
				ligne = br.readLine();
				
				String str[] = ligne.split("[:;]");
				String loginAdmin = str[3];
				
				if (loginAdmin.equals(loginAdminEntre)) {
					return true;
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
				
			
			return false;
		}
	
	public boolean controleMdp(JPasswordField mdp1){
		
		BufferedReader br = chercherFichier("C:\\ENV\\SBDD\\LoginMdp.txt");
		String ligne;
		String mdpAdminEntre = mdp1.getText();
		try {
			ligne = br.readLine();
			
			String str[] = ligne.split("[:;]");
			String mdpAdmin = str[5];
			
			if (mdpAdmin.equals(mdpAdminEntre)) {
				return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	
	}
	
