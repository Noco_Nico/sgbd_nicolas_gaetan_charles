package fr.afpa.controle;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class controleRegex {

	/**
	 * verif regex pour la creation de table
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static String createTable(String str) {
		
		try {
			Pattern p = Pattern.compile("create table \\w{3,25}");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				
				String [] spliter = entree.split(" ");
				return spliter[2];
			}
		} catch (PatternSyntaxException pse) {
		}
		return null;
	}// fonctionnel

	public static void messageErreur() {
		System.out.println("mauvaise synthaxe");
	}
	
	
	/**
	 * verif regex pour insertvalues
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static boolean insertIntoValues(String str) {
		try {
			Pattern p = Pattern.compile("insert into \\w{3,25} values ");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	/**
	 * verif regex pour select*from
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static boolean selectAllFrom(String str) {
		try {
			Pattern p = Pattern.compile("select \\* from \\w{3,25}");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	/**
	 * verif regex pour select from
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static boolean selectFrom(String str) {
		try {
			Pattern p = Pattern.compile("select \\w{3,25} from \\w{3,25}");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	/**
	 * verif regex pour selectOrderby
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static boolean selectOrderByFrom(String str) {
		try {
			Pattern p = Pattern.compile("select \\* from \\w{3,25} Order by \\w{3,25}");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel
		// TODO finir la spe ASC DESC

	/**
	 * verif regex pour update set
	 * 
	 * @param str
	 * @return true si regex validé
	 */
	public static boolean UpdateSet(String str) {
		try {
			Pattern p = Pattern.compile("update \\w{3,25} set ");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	public static boolean DeleteFrom(String str) {
		try {
			Pattern p = Pattern.compile("delete from \\w{3,25}");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	public static boolean DeleteFromWhere(String str) {
		try {
			Pattern p = Pattern.compile("delete from \\w{3,25} where ");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	public static boolean createDataBase(String str) {
		try {
			Pattern p = Pattern.compile("create database ");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

	public static boolean showTables(String str) {
		try {
			Pattern p = Pattern.compile("show tables");
			String entree = str;
			Matcher m = p.matcher(entree);
			while (m.find()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
		}
		return false;
	}// fonctionnel

}
//m.start() +" "+m.end()+" "+