package fr.afpa.ihm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;
import javax.swing.text.StyledEditorKit.BoldAction;

public class IhmCommandeSql extends JFrame {

	public void affichageFenetreSgbd() {

		setTitle("CDA SGBD");
		setSize(700, 800);

		JLabel sql = new JLabel("SQL-");
		sql.setFont(new Font("Arial", Font.BOLD, 12));
		JMenuBar barreMenu = new JMenuBar();
		JMenu menu1 = new JMenu("Fichier");
		JMenuItem itemMenu1_1 = new JMenuItem("Reset");
		JMenuItem itemMenu1_2 = new JMenuItem("Changer de Base De Données");
		JMenuItem itemMenu1_3 = new JMenuItem("Quitter");

		JMenu menu2 = new JMenu("?");
		JMenuItem itemMenu2_1 = new JMenuItem("A propos");

		JButton exe = new JButton("Exécuter");

		Box executer = Box.createHorizontalBox();
		exe.setAlignmentY(Component.TOP_ALIGNMENT);
		// exe.setAlignmentX(Component.RIGHT_ALIGNMENT);
		executer.add(exe);
		JLabel res = new JLabel("Résultat: " + sql.getText());
		Box resultat = Box.createHorizontalBox();
		res.setAlignmentY(Component.TOP_ALIGNMENT);
		// res.setAlignmentX(Component.LEFT_ALIGNMENT);
		resultat.add(res);

		// Box resEtExec = Box.createVerticalBox();
		// resEtExec.add(res, BorderLayout.WEST);
		// resEtExec.add(executer, BorderLayout.EAST);

		menu1.add(itemMenu1_1);
		menu1.add(itemMenu1_2);
		menu1.add(itemMenu1_3);

		menu2.add(itemMenu2_1);

		barreMenu.add(menu1);
		barreMenu.add(menu2);

		setJMenuBar(barreMenu);

		Container c = getContentPane();

		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1, 2));
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1, 1));
		JInternalFrame fenetreTable = new JInternalFrame("Liste des Tables");
		// fenetreTable.setSize(fenetreTable.getPreferredSize());
		JInternalFrame commandeSQL = new JInternalFrame("Commande SQL");
		// commandeSQL.setSize(commandeSQL.getPreferredSize());
		panel1.add(fenetreTable);
		panel1.add(commandeSQL);
		String[] listeTable = { "Table1", "Table1", "Table1", "Table1", "Table1", "Table1", "Table1", "Table1",
				"Table1", "Table1", "Table1", "Table1" };
		JList listeTable1 = new JList(listeTable);
		JTextField commandeSQL2 = new JTextField(20);
		commandeSQL.add(commandeSQL2);
		JScrollPane listeTable2 = new JScrollPane(listeTable1, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		Object[][] data = {
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" },
				{ "Gaëtan", "23 ans", "1m78", "Lille", "Chatain", "Noisette", "69kg", "BAC+3", "Masculin", "En couple",
						"59000", "CDA", "Roubaix" }

		};

		String title[] = { "Prenom", "Age", "Taille", "Ville", "Cheveux", "Yeux", "Poids", "Diplome", "Sexe",
				"Situation familiale", "Code Postal", "Formation", "Centre de Formation" };
		JTable affichageTableau = new JTable(data, title);

		JScrollPane affichageTableau1 = new JScrollPane(affichageTableau, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		affichageTableau.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panel2.add(affichageTableau1);
		fenetreTable.add(listeTable2);
		c.add(panel1, BorderLayout.NORTH);

		c.add(executer, BorderLayout.EAST);
		c.add(resultat, BorderLayout.WEST);

		c.add(panel2, BorderLayout.SOUTH);
		commandeSQL.setVisible(true);
		fenetreTable.setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);

	}

}
