package fr.afpa.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import fr.afpa.controle.ControleAuthentification;

public class IhmAuthentification extends JFrame implements ActionListener {

	JButton validation = new JButton("Ok");
	JButton annulation = new JButton("Annuler");
	JTextField login1 = new JTextField(10);
	JLabel erreur = new JLabel("Login ou Mot de passe erroné");
	JPasswordField mdp1 = new JPasswordField(10);

	public void affichageAuthentification() {

		annulation.addActionListener(this);
		validation.addActionListener(this);
		setTitle("Authentification");
		setSize(325, 150);

		JLabel login = new JLabel("Login:");
		login.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel mdp = new JLabel("Mot de passe:");
		mdp.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel phrase = new JLabel("Veuillez saisir votre login et votre mot de passe");
		 // A afficher lorsque login/mdp erroné
		Color rouge = new Color(255, 0, 0);
		erreur.setForeground(rouge);

		
		login1.setMaximumSize(login1.getPreferredSize());
		
		mdp1.setMaximumSize(mdp1.getPreferredSize());

		Box loginMdp = Box.createVerticalBox();
		Box texteField = Box.createVerticalBox();
		Box phrase1 = Box.createHorizontalBox();
		Box bouton = Box.createHorizontalBox();

		phrase1.add(phrase);

		loginMdp.add(login);
		loginMdp.add(Box.createVerticalStrut(10));
		loginMdp.add(mdp);
		;

		texteField.add(login1);
		texteField.add(Box.createVerticalStrut(10));
		texteField.add(mdp1);

		bouton.add(validation);
		// bouton.add(Box.createGlue());
		bouton.add(annulation);

		Box affichageSaisie = Box.createHorizontalBox();
		affichageSaisie.add(loginMdp);
		affichageSaisie.add(texteField);

		Box affichage = Box.createVerticalBox();
		affichage.add(phrase1, BorderLayout.CENTER);
		affichage.add(affichageSaisie);
		affichage.add(erreur);
		affichage.add(Box.createGlue());
		affichage.add(bouton);

		Container c = getContentPane();

		c.add(affichage, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // A ajouter dans le controle, Pour que la fenêtre soit
															// automatiquement fermée, (Pour une connexion réussie)

		erreur.setVisible(false);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == validation) {
				if (new ControleAuthentification().controleLogin(login1) == true
						&& new ControleAuthentification().controleMdp(mdp1) == true) {
				new IhmCommandeSql().affichageFenetreSgbd();
				this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				this.setVisible(false);

}
				else { erreur.setVisible(true);}
		}

		if (e.getSource() == annulation) {
			System.exit(0);
		}

	}

}
