package fr.afpa.commandes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import fr.afpa.controle.controleRegex;
import lombok.Getter;
import lombok.Setter;

public class CommandesSql {
	/**
		 * 
		 */
	@Getter
	@Setter
	private int id = 1;
	private String NomTable;
	private String nomColonne1 = null;
	private String nomColonne2 = null;
	private String nomColonne3 = null;

	public CommandesSql() {

	}

	public CommandesSql(String nomTable) {
		super();
		this.NomTable = nomTable;

	}

	public CommandesSql(String nomTable, int id, String nomColonne1, String nomColonne2, String nomColonne3) {
		super();
		this.id = id;
		this.nomColonne1 = nomColonne1;
		this.nomColonne2 = nomColonne2;
		this.nomColonne3 = nomColonne3;
		this.NomTable = nomTable;
	}

	/**
	 * Creation de Table
	 * 
	 * @param nomTable    : le nom de la table que l'utilisateur souhaite creer
	 * @param nomColonne1 : le nom de la premiere colonne que l'utilisateur souhaite
	 *                    creer
	 * @param nomColonne2 : le nom de la deuxieme colonne que l'utilisateur souhaite
	 *                    creer
	 * @param nomColonne3 : le nom de la troisieme colonne que l'utilisateur
	 *                    souhaite creer
	 */
	public void createTable(String nomTable, int id, String nomColonne1, String nomColonne2, String nomColonne3) {
		controleRegex.createTable(nomTable);
		siExiste(nomTable);
		FileWriter fw;
		try {
			fw = new FileWriter(nomTable + ".cda");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(id);
			if (nomColonne1 == null) {
				System.out.println("Veuillez entrer le(s) noms de colonne(s)");
			} else if (nomColonne2 == null)
				bw.write(nomColonne1+" ,");
			if (nomColonne2 != null) {
				bw.write(";" + nomColonne2+" ,");
				if (nomColonne3 != null) {
					bw.write(";" + nomColonne3+" ,");
					bw.newLine();
					bw.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Insere une nouvelle ligne dans le fichier choisi
	 * 
	 * @param nomTable    : le nom du fichier
	 * @param nomColonne1 : la valeur inserre dans la base de donnee a la colonne
	 *                    coisie
	 * @param nomColonne2 : la valeur inserre dans la base de donnee a la colonne
	 *                    coisie
	 * @param nomColonne3 : la valeur inserre dans la base de donnee a la colonne
	 *                    coisie
	 */
	public void insertIntoValues(String nomTable, int id, String nomColonne1, String nomColonne2, String nomColonne3) {
		controleRegex.insertIntoValues(nomTable);
		File f = new File(nomTable);
		if (f.exists()) {
			List<String> contenu = new ArrayList();
			for (String string : contenu) {
				contenu.addAll(lectureFichier(f.toString()));
			}
			id++;
			ecritureFichier(nomTable, id + " , ", true);
			ecritureFichier(nomTable, nomColonne1 + " , ", true);
			ecritureFichier(nomTable, nomColonne2 + " , ", true);
			ecritureFichier(nomTable, nomColonne3, true);
			newLine(nomTable, true);
		}
		
	}
	
/**
 * affiche la table demandée
 * @param nomTable : le nom de la table a afficher
 * @return la table avec une mise en page
 * @throws IOException
 */
	
	public List<String> selectAllFrom (String nomTable) throws IOException  {
		controleRegex.selectAllFrom(nomTable);
		List<String> contenu = new ArrayList<String>();
		FileReader fr = new FileReader(nomTable);
		BufferedReader br = new BufferedReader(fr);
		try {
			contenu.add( br.readLine().replace(",","\t\t|\t\t"));
			contenu.add("\n");
			while (br.ready()) {
			contenu.add( br.readLine().replace(",","\t\t|\t\t"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contenu;
		
	}
	/**
	 * affiche la colonne d'une table stipulee par l'utilisateur
	 * @param nomColonne : le nom de la colonne que souhaite afficher l'utilisateur
	 * @param nomTable : la table concernee
	 * @return : la colonne
	 * @throws IOException
	 */
	public List<String> selectFrom (String nomColonne, String nomTable) throws IOException {
		controleRegex.selectFrom(nomTable);
		List<String> contenu = new ArrayList<String>();
		FileReader fr = new FileReader(nomTable);
		BufferedReader br = new BufferedReader(fr);
		try {
			String [] nomChamp = br.readLine().split(contenu.toString(), ',');
			for (int i = 0; i<=nomChamp.length;i++) {
				if (nomColonne.equals(nomChamp[i])) {
					contenu.add(nomChamp[i]) ;
				}
			}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return contenu;
			
		}
	/**
	 * affiche la colonne d'une table stipulee par l'utilisateur que celui ci choisit de faire afficher dans un ordre croissant ou decroissant
	 * @param nomColonne : le nom de la colonne que souhaite afficher l'utilisateur
	 * @param nomTable : la table concernee
	 * @return : la colonne ordonnee selon les parametres rentres par l'utilisateur
	 * @throws IOException
	 */
	public List<String> selectOrderByFrom (String nomColonne, String nomTable, String sort) throws IOException {
		controleRegex.selectOrderByFrom(nomTable);
		List<String> contenu = new ArrayList<String>();
		FileReader fr = new FileReader(nomTable);
		BufferedReader br = new BufferedReader(fr);
		try {
			String [] nomChamp = br.readLine().split(contenu.toString(), ',');
			for (int i = 0; i<=nomChamp.length;i++) {
				if (nomColonne.equals(nomChamp[i])) {
					contenu.add(nomChamp[i]) ;
		if(sort.equals("ASC")){
			Collections.sort(contenu);
		}
		else {
			Collections.sort(contenu);
			Collections.reverse(contenu);	
		}
				}
			}
		}catch (IOException e) {
				e.printStackTrace();
			}	
			return contenu;
	}
	/**
	 * Permet l'attribution d'une nouvelle valeur au contenu d'une colonne définie par l'utilisateur
	 * @param nomTable : le nom de la table
	 * @param colonne : le nom du champ
	 * @param nouvelle_valeur : la valeur définie par l'utilisateur
	 * @return la table avec les valeurs modifiées
	 */
			
	public List<String> UpdateSet(String nomTable,String colonne, String nouvelle_valeur) {
		controleRegex.UpdateSet(nomTable);
		List<String> contenu = new ArrayList<String>();
		try {
		FileReader fr = new FileReader(nomTable);
		BufferedReader br = new BufferedReader(fr);
			String [] nomChamp = br.readLine().split(contenu.toString(), ',');
			for (int i = 0; i<=nomChamp.length;i++) {
				if (colonne.equals(nomChamp[i])) {
					br.readLine();
					contenu.add(nomChamp[i] =nouvelle_valeur) ;
				}
			}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return contenu;
			
		}
	/**
	 * Permet l'attribution d'une nouvelle valeur au contenu de deux colonnes définies par l'utilisateur
	 * @param nomTable : le nom de la table
	 * @param colonne1 : le nom du champ
	 * @param colonne2 : le nom du champ
	 * @param nouvelle_valeur : la valeur définie par l'utilisateur
	 * @return la table avec les valeurs modifiées
	 */
	public List<String> UpdateSetNomChamp1et2(String nomTable,String colonne1, String colonne2, String nouvelle_valeur1, String nouvelle_valeur2) {
		controleRegex.UpdateSet(nomTable);
		List<String> contenu = new ArrayList<String>();
		try {
		FileReader fr = new FileReader(nomTable);
		BufferedReader br = new BufferedReader(fr);
			String [] nomChamp = br.readLine().split(contenu.toString(), ',');
			for (int i = 0; i<=nomChamp.length;i++) {
				for (int j = 0; j <=nomChamp.length;i++) {
				if (colonne1.equals(nomChamp[i])) {
					contenu.add(nomChamp[i] = nouvelle_valeur1 ) ;
					if (colonne2.equals(nomChamp[j])) {
						contenu.add(nomChamp[j] = nouvelle_valeur2 ) ;
				}
				}
			}
			}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return contenu;
			
		}
	/**
	 * Creation d'une base de donnees
	 * 
	 * @param bdd : nom de la base de donnees
	 * @return : dossier cree au nom voulu par l'utilisateur
	 */
	public File createDataBase(String bdd) {
		
		controleRegex.createDataBase(bdd);
		
		if (!siExiste(bdd)) {
			File file = new File("C:\\ENV\\");
			file.mkdir();
			
			return file;
		}
		return null;

	}

	/**
	 * Suppression de la table, tout en conservant le nom des colonnes
	 * 
	 * @param nomTable : la table a effacer
	 * @return : une table vide
	 */

	public List<String> deleteFrom(String nomTable) {
		controleRegex.DeleteFrom(nomTable);
		BufferedReader br = null;
		List<String> contenuFichier = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(nomTable);
			br = new BufferedReader(fr);
			while (br.ready()) {
				contenuFichier.add(br.readLine());
			}
			for (int i = contenuFichier.size(); i > contenuFichier.size() + 1; i--) {
				contenuFichier.remove(i);
			}
		} catch (Exception e) {
			System.out.println("Erreur lecture");
		}
		return contenuFichier;
	}

	
	public boolean siExiste(String file) {
		
		File f = new File(file);
		
		if (f.exists() && !f.isDirectory()) {
			
			return false;
		}
		return true;
	}

	/**
	 * Cree une liste des tables contenues dans la base de donnees
	 * 
	 * @param folder : nom de la base de donnees
	 * @return : la liste des tables de dla base de donnees
	 */
	public List<String> ListerDossier(String folder) {

		ArrayList<String> fichiers = new ArrayList<>();
		File monDossier = new File(folder);
		File[] liste = monDossier.listFiles();

		for (File elementListe : liste) {
			if (elementListe.isFile()) {
				fichiers.add(elementListe.getName());
			}
		}
		return fichiers;
	}

	/**
	 * Lecteur generique de fichier
	 * 
	 * @param file : nom du fichier a lire
	 * @return : le contenu du fichier
	 */
	public List<String> lectureFichier(String file) {

		BufferedReader br = null;
		List<String> contenuFichier = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			while (br.ready()) {
				contenuFichier.add(br.readLine());
			}
		} catch (Exception e) {
			System.out.println("Erreur lecture");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("Erreur fermeture buffer");
				}
			}
		}
		return contenuFichier;
	}

	/**
	 * Ecriture generique de fichier
	 * 
	 * @param file     : nom du fichier a ecrire
	 * @param ecriture : boolean specifiant d'ecrire a la suite du fichier
	 * @return : booean specifiant que l'operation s'est deroulee correctement
	 */

	public boolean ecritureFichier(String file, String contenu, boolean ecriture) {
		boolean flag = true;
		BufferedWriter bw = null;
		try {
			File fichier = new File(file);
			File dir = fichier.getParentFile();
			dir.mkdirs();
			FileWriter fw = new FileWriter(file, ecriture);
			bw = new BufferedWriter(fw);
			bw.write(contenu);
		} catch (Exception e) {
			flag = false;
			System.out.println("Probleme ecriture fichier");
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					flag = false;
					System.out.println("Probleme ecriture fichier");
				}
			}
		}
		return flag;
	}

	/**
	 * ecriture d'une nouvelle ligne dans un fichier
	 * 
	 * @param file     : le nom du fichier dans lequel ecrire
	 * @param ecriture : booean permettant d'ecrire a la suite du fichier
	 * @return
	 */
	public boolean newLine(String file, boolean ecriture) {
		boolean flag = true;
		BufferedWriter bw = null;
		try {
			File fichier = new File(file);
			File dir = fichier.getParentFile();
			dir.mkdirs();
			FileWriter fw = new FileWriter(file, ecriture);
			bw = new BufferedWriter(fw);
			bw.newLine();
			bw.close();
		} catch (Exception e) {
			flag = false;
			System.out.println("Probleme ecriture fichier");
		}
		return flag;
	}
}
